const express = require("express"),
  app = express(),
  server = require("http").createServer(app),
  sticky = require("sticky-session"),
  cluster = require("cluster");

if (!sticky.listen(server, 3000)) {
  server.once("listening", function() {});
  //Revive the worker when it dies
  cluster.on("exit", (worker, code, signal) => {
    console.log("dead");
    if (code == 200) {
      cluster.fork();
    }
  });
} else {
  const bodyParser = require("body-parser"),
    cookieParser = require("cookie-parser"),
    session = require("express-session"),
    config = require("./config"),
    helmet = require("helmet"),
    passport = require("passport");

  //initialize server.
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(helmet());
  app.set("view engine", "ejs");
  app.use(cookieParser());
  app.use(
    session({
      secret: config.sessionOption.secret,
      resave: true,
      saveUninitialized: false
    })
  );
  app.use("/rsrc", express.static("rsrc"));

  //routes
  app.get("/", (req, res) => {
    res.render("index");
  });
}
